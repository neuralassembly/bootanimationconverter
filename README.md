Galaxy Nexus 用の bootanimation.zip を Nexus S 用に変換する Java アプリケーションです。

ダブルクリックで起動し、bootanimation.zip をドラッグアンドドロップすると、同じディレクトリに ns というディレクトリを作り、変換後の bootanimation.zip を保存します。jar がアーカイバに関連付けられている場合起動しませんので、右クリックから Java で起動するよう選択して下さい。

行う処理は

・desc.txt の1行目にて、解像度を 480x800 に書き換える

・各画像のアスペクト比が崩れないよう、上下をカット

・画像サイズは 480x800 か 240x400 のどちらかを選択

・再 zip 

です。処理の過程で「_ns」というディレクトリを作成し、最後に削除しますので注意。

ファイル名は bootanimation*.zip に対応しています。

動作確認は Windows / Ubuntu の Java6 にて、「ひつじさん」テーマの bootanimation.zip でのみ行いました。Mac OS Xでも動作しましたが、Finderを再読み込みしないと反映されません。

