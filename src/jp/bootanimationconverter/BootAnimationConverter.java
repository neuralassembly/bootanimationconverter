package jp.bootanimationconverter;

import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;
import java.util.zip.CRC32;
import java.net.URI;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Graphics2D;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetListener;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DnDConstants;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;

public class BootAnimationConverter  extends JFrame implements ActionListener {
	
	private static final boolean DEBUG=false;
	
	private static final int FORMAT_SAME = 0x00;
	private static final int FORMAT_PNG = 0x01;
	private static final int FORMAT_JPG = 0x02;
	
	private static final int SIZE_NS_FULL_W = 480;
	private static final int SIZE_NS_FULL_H = 800;
	private static final int SIZE_NS_HALF_W = 240;
	private static final int SIZE_NS_HALF_H = 400;
	
	private int mFormat;
	private int mTargetW=SIZE_NS_FULL_W;
	private int mTargetH=SIZE_NS_FULL_H;
	
	private int mImageW;
	private int mImageH;
	
	private float mQuality;
	
	private JMenuBar menuBar;
	private JMenu menuFormat;
	private JCheckBoxMenuItem menuFormatSame;
	private JCheckBoxMenuItem menuFormatPng;
	private JCheckBoxMenuItem menuFormatJpg;
	
	private JMenu menuSize;
	private JCheckBoxMenuItem menuSizeFull;
	private JCheckBoxMenuItem menuSizeHalf;
	
	private JMenu menuQuality;
	private JCheckBoxMenuItem menuQuality100;
	private JCheckBoxMenuItem menuQuality90;
	private JCheckBoxMenuItem menuQuality80;
	private JCheckBoxMenuItem menuQuality70;
	private JCheckBoxMenuItem menuQuality60;
	private JCheckBoxMenuItem menuQuality50;
	private JCheckBoxMenuItem menuQuality40;

	BootAnimationConverter() {
        getContentPane().setLayout(new FlowLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("BootAnimationConverter");

    	menuBar = new JMenuBar();
    	menuFormat = new JMenu("Image Format");
    	menuFormatSame = new JCheckBoxMenuItem("Same as input",true);
    	menuFormatSame.addActionListener(this);
    	menuFormatPng = new JCheckBoxMenuItem("PNG");
    	menuFormatPng.addActionListener(this);
    	menuFormatJpg = new JCheckBoxMenuItem("JPEG");
    	menuFormatJpg.addActionListener(this);
    	ButtonGroup bgFormat = new ButtonGroup();
    	
    	menuSize = new JMenu("Image Size");
    	menuSizeFull = new JCheckBoxMenuItem("480x800",true);
    	menuSizeFull.addActionListener(this);
    	menuSizeHalf = new JCheckBoxMenuItem("240x400");
    	menuSizeHalf.addActionListener(this);
    	ButtonGroup bgSize = new ButtonGroup();
    	menuQuality = new JMenu("Quality(jpg)");
    	menuQuality100 = new JCheckBoxMenuItem("100");
    	menuQuality100.addActionListener(this);
    	menuQuality90 = new JCheckBoxMenuItem("90");
    	menuQuality90.addActionListener(this);
    	menuQuality80 = new JCheckBoxMenuItem("80",true);
    	menuQuality80.addActionListener(this);
    	menuQuality70 = new JCheckBoxMenuItem("70");
    	menuQuality70.addActionListener(this);
    	menuQuality60 = new JCheckBoxMenuItem("60");
    	menuQuality60.addActionListener(this);
    	menuQuality50 = new JCheckBoxMenuItem("50");
    	menuQuality50.addActionListener(this);
    	menuQuality40 = new JCheckBoxMenuItem("40");
    	menuQuality40.addActionListener(this);
    	ButtonGroup bgQuality = new ButtonGroup();
        
    	getRootPane().setJMenuBar(menuBar);
    	
    	menuBar.add(menuFormat);
    	menuFormat.add(menuFormatSame);
    	menuFormat.add(menuFormatPng);
    	menuFormat.add(menuFormatJpg);
    	bgFormat.add(menuFormatSame);
    	bgFormat.add(menuFormatPng);
    	bgFormat.add(menuFormatJpg);
    	
    	menuBar.add(menuSize);
    	menuSize.add(menuSizeFull);
    	menuSize.add(menuSizeHalf);
    	bgSize.add(menuSizeFull);
    	bgSize.add(menuSizeHalf);
    	
    	menuBar.add(menuQuality);
    	menuQuality.add(menuQuality100);
    	menuQuality.add(menuQuality90);
    	menuQuality.add(menuQuality80);
    	menuQuality.add(menuQuality70);
    	menuQuality.add(menuQuality60);
    	menuQuality.add(menuQuality50);
    	menuQuality.add(menuQuality40);
    	bgQuality.add(menuQuality100);
    	bgQuality.add(menuQuality90);
    	bgQuality.add(menuQuality80);
    	bgQuality.add(menuQuality70);
    	bgQuality.add(menuQuality60);
    	bgQuality.add(menuQuality50);
    	bgQuality.add(menuQuality40);
    	
        JLabel label = new JLabel();
        label.setText("<html>Galaxy Nexus 用の bootanimation.zip を<br>ドラッグアンドドロップしてください。<br>Nexus S 用の ns/bootanimation.zip を生成します</html>");
        label.setHorizontalAlignment(JLabel.CENTER);
        BorderLayout l = new BorderLayout();
        setLayout(l);
        add(label);
        
        setSize(350, 200);
        setVisible(true);
        
        if(menuFormatSame.isSelected()){
        	mFormat = FORMAT_SAME;
        }else if(menuFormatPng.isSelected()){
        	mFormat = FORMAT_PNG;
        }else{
        	mFormat = FORMAT_JPG;
        }
        		
        if(menuSizeFull.isSelected()){
        	mImageW = SIZE_NS_FULL_W;
        	mImageH = SIZE_NS_FULL_H;
        }else{
        	mImageW = SIZE_NS_HALF_W;
        	mImageH = SIZE_NS_HALF_H;
        }
        		
        if(menuQuality100.isSelected()){
        	mQuality = 1.0f;
        }else if(menuQuality90.isSelected()){
        	mQuality = 0.9f;
        }else if(menuQuality80.isSelected()){
        	mQuality = 0.8f;
        }else if(menuQuality70.isSelected()){
        	mQuality = 0.7f;
        }else if(menuQuality60.isSelected()){
        	mQuality = 0.6f;
        }else if(menuQuality50.isSelected()){
        	mQuality = 0.5f;
        }else{
        	mQuality = 0.4f;
        }
        
        if(DEBUG) System.err.println(""+mImageW+"x"+mImageH);
        
        DropTargetListener dtl = new DropTargetAdapter() {
            @Override public void dragOver(DropTargetDragEvent dtde) {
                if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
                		|| dtde.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    dtde.acceptDrag(DnDConstants.ACTION_COPY);
                    return;
                }
                dtde.rejectDrag();
            }
            @Override
            public void drop(DropTargetDropEvent dtde) {
                try{
                    if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)
                    		|| dtde.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                        dtde.acceptDrop(DnDConstants.ACTION_COPY);
                        Transferable transferable = dtde.getTransferable();
                        List<File> list = null;
                        if(dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)){ // Windows
                        	list = ((List<File>)transferable.getTransferData(DataFlavor.javaFileListFlavor));
                        }else if(dtde.isDataFlavorSupported(DataFlavor.stringFlavor)){ // Linux
                        	String str = (String) transferable.getTransferData(DataFlavor.stringFlavor);
                        	String lineSep = System.getProperty("line.separator");
                        	String[] fileList = str.split(lineSep);
                        	list = new ArrayList<File>();
                        	for(int i=0 ; i<fileList.length ; i++){
                        		try{
                        			File f = new File(new URI(fileList[i].replaceAll("[\r\n]", "")));
                        			list.add(f);
                        			if(DEBUG) System.err.println(f.getAbsolutePath());
                        		}catch(Exception e){}
                        	}
                        }
                        for(Object o: list) {
                            if(o instanceof File) {
                                File file = (File) o;                                
                                convertGNBAtoNSBA(file);                             
                            }
                        }
                        dtde.dropComplete(true);
                        return;
                    }
                }catch(UnsupportedFlavorException ufe) {
                    ufe.printStackTrace();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                dtde.rejectDrop();
            }
        };

        new DropTarget(this, DnDConstants.ACTION_COPY, dtl, true);
        
    }
	
	@Override
	public void actionPerformed(ActionEvent event) {
		if(event.getSource() == menuFormatSame){
			mFormat = FORMAT_SAME;
		}else if(event.getSource() == menuFormatPng){
			mFormat = FORMAT_PNG;
		}else if(event.getSource() == menuFormatJpg){
			mFormat = FORMAT_JPG;
		}else if(event.getSource() == menuSizeFull){
			mImageW = SIZE_NS_FULL_W;
			mImageH = SIZE_NS_FULL_H;
		}else if(event.getSource() == menuSizeHalf){
			mImageW = SIZE_NS_HALF_W;
			mImageH = SIZE_NS_HALF_H;
		}else if(event.getSource() == menuQuality100){
			mQuality = 1.0f;
		}else if(event.getSource() == menuQuality90){
			mQuality = 0.9f;
		}else if(event.getSource() == menuQuality80){
			mQuality = 0.8f;
		}else if(event.getSource() == menuQuality70){
			mQuality = 0.7f;
		}else if(event.getSource() == menuQuality60){
			mQuality = 0.6f;
		}else if(event.getSource() == menuQuality50){
			mQuality = 0.5f;
		}else if(event.getSource() == menuQuality40){
			mQuality = 0.4f;
		}
		
	}
	
	public void convertGNBAtoNSBA(File file){

		if(!file.getName().startsWith("bootanimation")
				|| !file.getName().endsWith(".zip")){
			return;
		}
		
		String tmpDirName = "_ns";
		String targetDirName = "ns";
		
		try{
			ZipInputStream in = new ZipInputStream(new BufferedInputStream(new FileInputStream(file)));
	         
			ZipEntry zipEntry;
			int num;
			
			String tmpDir_s = file.getParentFile().getAbsolutePath()+ File.separator + tmpDirName;
			File tmpDir = new File(tmpDir_s);
			
			if(tmpDir.exists()){
				delete(tmpDir);
			}
			tmpDir.mkdirs();
			
			while( (zipEntry = in.getNextEntry()) != null )
			{
            	 File nf = new File(tmpDir_s + File.separator + zipEntry.getName());
            	 
            	 if(getSuffix(nf)==null){
            		 continue;
            	 }
            	 
            	 // extract the target from zip file
            	 if(!nf.getParentFile().exists()){
            		 nf.getParentFile().mkdirs();
            	 }
            	 BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(nf));
            	 while( (num = in.read()) != -1 )
            	 {
            		 out.write(num);
            	 }

            	 // close files
            	 in.closeEntry();
            	 out.close();
            	 
            	 // modify GN size -> NS size
            	 if("desc.txt".equals(nf.getName())){

                     BufferedReader br = new BufferedReader(new FileReader(nf));
                     
                     File nf_mod = new File(nf.getParentFile().getAbsolutePath()
                    		 				+ File.separator +"_desc.txt");
                     BufferedWriter bw = new BufferedWriter(new FileWriter(nf_mod));
                     
                     String line;
                     // 1st line
                     if((line = br.readLine()) != null){
                    	 StringTokenizer st = new StringTokenizer(line);
                    	 int orgw = Integer.parseInt(st.nextToken());
                    	 int orgh = Integer.parseInt(st.nextToken());
                    	 int fps = Integer.parseInt(st.nextToken());
                    	 line = ""+mTargetW+" "+mTargetH+" "+fps;
                         bw.write(line);
                         bw.newLine();
                     }
                     // remaining
                     while ((line = br.readLine()) != null) {
                         bw.write(line);
                         bw.newLine();
                     }
                     br.close();    
                     bw.close();
                     
                     // delete old file
                     nf.delete();
                     nf_mod.renameTo(nf);
            	 }
            	 
            	 String orgSuffix = getSuffix(nf);
            	 File nf_target = new File(nf.getAbsolutePath());
            	 // resize image 
            	 if("png".equals(orgSuffix) 
            		|| "PNG".equals(orgSuffix) 
            		|| "jpg".equals(orgSuffix) 	 
            		|| "JPG".equals(orgSuffix) 
            		|| "jpeg".equals(orgSuffix) 	 
            		|| "JPEG".equals(orgSuffix) ){
            		 
            		 BufferedImage readImage = ImageIO.read(nf);
            			 
            		 int w = readImage.getWidth();
            		 int h = readImage.getHeight();

            		 double wrate = w/(double)mTargetW;
            		 double hrate = h/(double)mTargetH;
            			 
            		 int w_mod = mImageW;
        			 int h_mod = mImageH;
        			 int cropsize_w=0;
            		 int cropsize_h=0;
            		 if(hrate>wrate){
            			 cropsize_h = h-(int)(w*mTargetH/(double)mTargetW);;
            		 }else if(wrate>hrate){
            			 cropsize_w = w-(int)(h*mTargetW/(double)mTargetH);;
            		 }

            		 BufferedImage writeImage = new BufferedImage(w_mod, h_mod, 
            				 BufferedImage.TYPE_INT_BGR);
                         
            		 Graphics2D g = writeImage.createGraphics();
                         
            		 g.drawImage(readImage,
            				 0,0,w_mod-1,h_mod-1,
            				 cropsize_w/2,cropsize_h/2,w-cropsize_w/2-1,h-cropsize_h/2-1,
            				 this);

            		 File nf_mod = new File(nf.getParentFile().getAbsolutePath()
            				 + File.separator +"_"+nf.getName());
                         
            		 if(mFormat==FORMAT_PNG 
            				 || (mFormat==FORMAT_SAME && ("png".equals(orgSuffix) || "PNG".equals(orgSuffix)) )) 
            		 {
            			 if(!"png".equals(orgSuffix) && !"PNG".equals(orgSuffix)){
            				 nf_mod = replaceSuffix(nf_mod,"png");
            				 nf_target = replaceSuffix(nf_target,"png");
            			 }
            			 ImageIO.write(writeImage, "png", nf_mod);
            		 }
            		 else if(mFormat==FORMAT_JPG 
            				 || (mFormat==FORMAT_SAME && ("jpg".equals(orgSuffix) || "JPG".equals(orgSuffix)|| "jpeg".equals(orgSuffix)|| "JPEG".equals(orgSuffix)) ))
            		 {
            			 if(!"jpg".equals(orgSuffix) && !"JPG".equals(orgSuffix)
            					 && !"jpeg".equals(orgSuffix) && !"JPEG".equals(orgSuffix)){
            				 nf_mod = replaceSuffix(nf_mod,"jpg");
            				 nf_target = replaceSuffix(nf_target,"jpg");
            			 }
                        	 
            			 Iterator<ImageWriter> iter = ImageIO.getImageWritersByFormatName("jpeg");
            			 ImageWriter writer = (ImageWriter)iter.next();
            			 ImageWriteParam iwp = writer.getDefaultWriteParam();
            			 iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            			 iwp.setCompressionQuality(mQuality);
            			 FileImageOutputStream output = new FileImageOutputStream(nf_mod);
            			 writer.setOutput(output);
            			 IIOImage image = new IIOImage(writeImage, null, null);
            			 writer.write(null, image, iwp);
            			 writer.dispose();
            			 output.close();
            		 }
                         
            		 // delete old file
            		 nf.delete();
            		 nf_mod.renameTo(nf_target);
            	 }
			} 

			// pack to a zip file
			String outBA_s = file.getParentFile().getAbsolutePath()+ File.separator 
								+ targetDirName + File.separator + file.getName() ;
			
			File zipf  = new File(outBA_s);

			if(!zipf.getParentFile().exists()){
				zipf.getParentFile().mkdirs(); // create target dir
			}
			if(zipf.exists()){
				zipf.delete();
			}
			
			File[] files = { tmpDir };
			ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipf));
			zos.setMethod(ZipOutputStream.STORED); 
			try {
				encode(zos, files, tmpDir.getAbsolutePath().length()+1);
			} finally {
				zos.close();
			}
			
			delete(tmpDir);

			
		}catch(Exception ie){
			ie.printStackTrace();
		}		
	}
	
	public String getSuffix(File path) {
	    if (path.isDirectory()) {
	        return null;
	    }
	    String fileName = path.getName();
	    int lastDotPosition = fileName.lastIndexOf(".");
	    if (lastDotPosition != -1) {
	        return fileName.substring(lastDotPosition + 1);
	    }
	    return null;
	}
	
	public File replaceSuffix(File path, String suff) {
	    if (path.isDirectory()) {
	        return null;
	    }
	    String fileName = path.getName();
	    int lastDotPosition = fileName.lastIndexOf(".");
	    
	    String fileNameNew =fileName.substring(0,lastDotPosition+1)+suff;
	    if (lastDotPosition != -1) {
	        return new File(path.getParentFile().getAbsolutePath() + File.separator +fileNameNew );
	    }
	    return null;
	}
	
	static byte[] buf = new byte[1024];
	static void encode(ZipOutputStream zos, File[] files, int pos) throws Exception {
		
		for (File f : files) {

			if (f.isDirectory()) {
				encode(zos, f.listFiles(), pos);
			} else {

				String path = f.getPath().substring(pos, f.getPath().length());
				path = path.replace("\\","/"); // for Windows

				if(DEBUG) System.err.println(path);
				
				ZipEntry ze = new ZipEntry(path);

				ze.setSize(f.length());	//set data size

				CRC32 crc = new CRC32();
				BufferedInputStream input =
						new BufferedInputStream(new FileInputStream(f));
				int b;
				while ((b = input.read()) != -1) {
					crc.update(b);
				}
				input.close();
				ze.setCrc(crc.getValue());	//set CRC
				
				zos.putNextEntry(ze);

				InputStream is = new BufferedInputStream(new FileInputStream(f));

				for (;;) {
					int len = is.read(buf);
					if (len < 0) break;
					zos.write(buf, 0, len);
				}
				is.close();
				
			}
		}
	}
	static private void delete(File f){
		if( f.exists()==false ){
			return ;
		}

		if(f.isFile()){
			f.delete();
		}
			
		if(f.isDirectory()){
			File[] files=f.listFiles();
			for(int i=0; i<files.length; i++){
				delete( files[i] );
			}
			f.delete();
		}
	}
	
    public static void main(String [] args) {
        new BootAnimationConverter();
    }


}
